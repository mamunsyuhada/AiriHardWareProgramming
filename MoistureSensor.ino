void MoistureSensor() {
  for (int i = 0; i < readMultipler; i++)
  {
    high_time = pulseIn(pinLengas, HIGH);
    low_time = pulseIn(pinLengas, LOW);
    time_period = high_time + low_time;
    time_period = time_period / 1000;
    frequency = 1000 / time_period;
    freqTotal += frequency;
  }
  freqTotal = freqTotal / readMultipler;
  moisture = freqTotal - 30585;
  moisture = moisture / (-539.23);

  if (moisture < 0) {
    moisture = 0;
  }

  if (moisture >= 50) {
      valve = "Off";
    }
    else if (moisture <= 15 ) {
      valve = "On";
    }
}
