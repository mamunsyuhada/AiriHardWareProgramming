void InsertToCloud() {
  WiFiClient client;

  const int httpPort = 80;

  Serial.println("- connecting to pushing server: " + String(WEBSITE));

  if (!client.connect(WEBSITE, httpPort)) {
    Serial.println("gak ada koneksi internet");
    return;
  }

  Serial.println("Jooss terhubung ke server");

  String data = "/pushingbox?";
  data += "devid=";
  data += "vD296DAE41A201AE";
  data += "&temp=" + String(temp_c);
  data += "&rh=" + String(humidity);
  data += "&lumen=" + String(ldr);
  data += "&moisture=" + String(moisture);
  data += "&nutrition=" + String(nutrisiFinal);
  data += "&airi=" + String (AlamatAiri);
  data += "&valve=" + valve;

  Serial.println("Harap bersabar, si doi mencoba kirim data ke server");
  Serial.println("nih datanya");
  Serial.println(data);

  Serial.print("Requesting URL: ");
  Serial.println(data);

  // This will send the request to the server
  client.print(String("GET ") + data + " HTTP/1.1\r\n" +
               "Host: " + WEBSITE + "\r\n" +
               "Connection: close\r\n\r\n");
  unsigned long timeout = millis();
  while (client.available() == 0) {
    if (millis() - timeout > 5000) {
      Serial.println(">>> Client Timeout !");
      client.stop();
      return;
    }
  }

  //Fetching data istilahnya, nginjen data di pushing box
  while (client.available()) {
    // Kirim ke serial data yang di injennya
    String fetching = client.readStringUntil('\r');
    Serial.print(fetching);
    Serial.print("Data terkirim :) ");
  }

  Serial.println();
  Serial.println("closing connection");
}

