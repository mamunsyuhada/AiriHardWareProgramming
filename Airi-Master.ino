/*=================== Wifi ke Kloud Konfig ==================*/

#include <ESP8266WiFi.h>
const char* WEBSITE = "api.pushingbox.com"; //pushingbox API server
const char* devid = "vD296DAE41A201AE"; //device ID from Pushingbox
#define WIFI_SSID "proton" // your ssid
#define WIFI_PASSWORD "123asdfghqw" // your ssid-password lurd

/*=================== SHT Temp & Humidity Konfig ==================*/

#include <SHT1x.h>
#define dataPin  0
#define clockPin 2
SHT1x sht1x(dataPin, clockPin);
int temp_c;
float temp_f;
int humidity;

/*=================== Xbee Valve Konofig ==================*/
#include <SoftwareSerial.h>
SoftwareSerial xbeeSensor(12, 14); // RX, TX

/*=================== Lengas Tanah - Nutrisi Konfig ==================*/
#define pinLengas 15
int high_time;
int low_time;
float time_period;
float frequency;
float freqTotal;
int readMultipler = 50;
int moisture;

/*=================== 4051 Konfig ==================*/
#define muxInput A0
#define addrA 5
#define addrB 4

byte A = 0, B = 0;
byte channel;
byte totSensor = 2;
uint16_t nutrisi, ldr;
int nutrisiFinal;
uint16_t *adcArray[2] = {&nutrisi, &ldr};

/*=================== Valve ==================*/
String valve;

/*=================== Alamat Airi ==================*/
const String AlamatAiri = "1";

void setup() {
  Serial.begin(9600);
  xbeeSensor.begin(9600);

  /*=================== Selector 4051 ==================*/
  pinMode(addrA, OUTPUT);
  pinMode(addrB, OUTPUT);

  pinMode(pinLengas, INPUT);
  pinMode(A0, INPUT); //Analog dari 4051

  //Konek ke WIFI
  Serial.println(WIFI_SSID);
  Serial.flush();

  WiFi.mode(WIFI_STA);

  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  Serial.print("connecting");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }
  Serial.println();
  Serial.print("connected: ");
  Serial.println(WiFi.localIP());
}

void loop() {
  delay(5000);
  MoistureSensor();
  SuhuDanHumidity();
  KompilasiSensor();
  CekSerial();
  XbeeWrite();
  InsertToCloud();
}
