void KompilasiSensor() {

  for (channel = 0; channel < totSensor; channel++) {
    A = bitRead(channel, 0); // ambil biner bit 0 untuk nilai A
    B = bitRead(channel, 1); // ambil biner bit 1 untuk nilai B

    digitalWrite(addrA, A);
    digitalWrite(addrB, B);

    *adcArray[channel] = analogRead(muxInput);
  }
  nutrisiFinal = -2766 * (log(freqTotal)) + 28376;
  //int nutrisi = nutrisiFinal;
}
